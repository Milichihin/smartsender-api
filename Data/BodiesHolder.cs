﻿using Newtonsoft.Json;
using smartsender_api.Requests;
using System.Collections.Generic;
using System.Net.Http;

namespace smartsender_api.Data
{
    public class BodiesHolder
    {
        private static RequestBodyToUpdateContactNew _updateContactBody = new RequestBodyToUpdateContactNew
        {
            contactListId = DefaultContactListData.contactListId,
            webHookUri = DefaultContactListData.webHookUri,
            upsert = false,
            contacts = new List<Contact>()
            {
                new Contact()
                {
                    contact = "+15555555",
                    name = "John Doe",
                    email = "user@example.com",
                    phoneNumber = "+15555555",
                    userId = "myUserId",
                    active = true,
                    emailSubscribe = true,
                    smsSubscribe = true,
                    desktopWebPushSubscribe = true,
                    mobileWebPushSubscribe = true,
                    telegramSubscribe = true,
                    firstName = "John",
                    lastName = "Doe",
                    contactBirthday = "1990-01-01",
                    contactLanguage = "en",
                    contactGender = 2,
                    contactTimezone = "Europe/Tallinn",
                    contactScore = 100,
                    avatarLink = "img.smartsender.io/g/06c38b4568.jpg",
                    facebookLink = "facebook.com/smartsender.io",
                    instagramLink = "instagram.com/smartsender",
                    linkedInLink = "linkedin.com/company/smartsender",
                    twitterLink = "twitter.com/smart_sender",
                    tiktokLink = "tiktok.com/smartsender",
                    sourceId = "Source_ID_from_SmartSender",
                    customerSourceId = "Customer_Source_ID_From_Your_Platform",
                    cac = new Cac()
                    {
                        value = "2.05",
                        currency = "USD"
                    },
                    variables = new List<Variables>()
                    {
                        new Variables()
                        {
                            name = "newVariableName",
                            value = "newVariableValue"
                        }
                    }
                }
            }
        };

        public static StringContent updateContactBodyJSON = new StringContent(
               JsonConvert.SerializeObject(
                   _updateContactBody,
                   Formatting.Indented));
    }
}
