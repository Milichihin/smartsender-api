﻿namespace smartsender_api.Data
{
    public class UriData
    {
        private static readonly string _tokenURN = "?key=ZmRkNmMyMTQxOGU2MWU4NGVjOGU1NDg0MTJkZDYxZDc=";
        public static string baseURI = "https://api.smartsender.io/v3";
        public static string updateContactDataURI = "/contacts/update" + _tokenURN;
    }
}
