﻿using System.Threading.Tasks;
using System.Net.Http;
using smartsender_api.Core;

namespace smartsender_api.Business
{
    public class CustomAPIMethods
    {
        public static async Task<string> GetBodyAfterPostRequest(string uri, HttpContent body)
        {
            var response = await ClientHolder.restClient.PostAsync(uri, body);

            var content = response.Content.ReadAsStringAsync();

            return await content;
        }
    }
}
