﻿using System.Net.Http;

namespace smartsender_api.Core
{
    public class ClientHolder
    {
        private static HttpClient _instance;

        public static HttpClient restClient
        {
            get
            {
                return _instance ?? (_instance = new HttpClient());
            }
        }
    }
}
