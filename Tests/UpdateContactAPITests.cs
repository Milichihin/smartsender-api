﻿using Newtonsoft.Json;
using NUnit.Framework;
using smartsender_api.Business;
using smartsender_api.Data;
using smartsender_api.Responses;

namespace smartsender_api.Tests
{
    public class UpdateContactAPITests : BaseApiTest
    {
        [Test]
        [Category("APITests")]
        public void SuccessfulUpdateContactDataAPI_Test()
        {
            Assert.IsTrue(_responseObject.result);
        }

        [Test]
        [Category("APITests")]
        public void IsTokenValidToUpdateContactDataAPI_Test()
        {
            Assert.AreEqual("Invalid authorization token!", _responseObject.errors[0]);
        }
    }
}
