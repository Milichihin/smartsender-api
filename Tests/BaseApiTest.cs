﻿using Newtonsoft.Json;
using NUnit.Framework;
using smartsender_api.Business;
using smartsender_api.Core;
using smartsender_api.Data;
using smartsender_api.Responses;

namespace smartsender_api.Tests
{
    public class BaseApiTest
    {
        protected ResponseBodyAferUpdateContact _responseObject;

        [SetUp]
        public void SetResponseBody()
        {
            string uri = UriData.baseURI + UriData.updateContactDataURI;

            var updateResponse = CustomAPIMethods.GetBodyAfterPostRequest(
                uri, BodiesHolder.updateContactBodyJSON
                );

            _responseObject = JsonConvert
                .DeserializeObject<ResponseBodyAferUpdateContact>(updateResponse.Result);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            ClientHolder.restClient.Dispose();
        }
    }
}
