﻿using System.Collections.Generic;

namespace smartsender_api.Responses
{
    public class ResponseBodyAferUpdateContact
    {
        public bool result { get; set; }
        public string requestId { get; set; }
        public List<string> errors { get; set; }
    }
}
