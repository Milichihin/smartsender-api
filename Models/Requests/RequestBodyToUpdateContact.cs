﻿using System.Collections.Generic;

namespace smartsender_api.Requests
{
    public class RequestBodyToUpdateContactNew
    {
        public string contactListId { get; set; }
        public string webHookUri { get; set; }
        public bool upsert { get; set; }
        public List<Contact> contacts { get; set; }
    }

    public class Contact
    {
        public string contact { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string userId { get; set; }
        public bool active { get; set; }
        public bool emailSubscribe { get; set; }
        public bool smsSubscribe { get; set; }
        public bool desktopWebPushSubscribe { get; set; }
        public bool mobileWebPushSubscribe { get; set; }
        public bool telegramSubscribe { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string contactBirthday { get; set; }
        public string contactLanguage { get; set; }
        public int contactGender { get; set; }
        public string contactTimezone { get; set; }
        public int contactScore { get; set; }
        public string avatarLink { get; set; }
        public string facebookLink { get; set; }
        public string instagramLink { get; set; }
        public string linkedInLink { get; set; }
        public string twitterLink { get; set; }
        public string tiktokLink { get; set; }
        public string sourceId { get; set; }
        public string customerSourceId { get; set; }
        public Cac cac { get; set; }
        public List<Variables> variables { get; set; }
    }

    public class Variables
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Cac
    {
        public string value { get; set; }
        public string currency { get; set; }
    }
}
